#%matplotlib inline

from matplotlib import pyplot as plt
import cv2
from mtcnn.mtcnn import MTCNN
import numpy as np
import os,re
import tensorflow as tf
import time
import atexit

model = "20180408-102900"
image_size = 160
#image_file = "01.jpg"
images_placeholder = None
embeddings = None
phase_train_placeholder = None
sess=tf.Session(config=tf.ConfigProto(log_device_placement=True))


def overlay(img, bbs, labels):
    for bounding_box, label in zip(bbs,labels):
        cv2.rectangle(img,
                  (bounding_box[0], bounding_box[1]),
                  (bounding_box[0]+bounding_box[2], bounding_box[1] + bounding_box[3]),
                  (0,255,255),
                  3)
        cv2.line(img, (bounding_box[0], bounding_box[1]),
                  (bounding_box[0]+bounding_box[2], bounding_box[1] + bounding_box[3]),(255,0,0),3)
        font = cv2.FONT_HERSHEY_TRIPLEX
        cv2.putText(img, label,(bounding_box[0], bounding_box[1] + bounding_box[3]), font, 3, (0,200,200), 4, cv2.LINE_AA)
    return img
def extract_face(img,bb):
    return img[bb[1]:bb[1]+bb[3],bb[0]:bb[0]+bb[2]]

def prewhiten(x):
    mean = np.mean(x)
    std = np.std(x)
    std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
    y = np.multiply(np.subtract(x, mean), 1/std_adj)
    return y

def resize_with_pad(im,desired_size):
    old_size = im.shape[:2] # old_size is in (height, width) format

    ratio = float(desired_size)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h//2, delta_h-(delta_h//2)
    left, right = delta_w//2, delta_w-(delta_w//2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
        value=color)
    return new_im
def prep_img(image, image_size, do_prewhiten=True):
    if image.ndim == 2:
            image = to_rgb(image)
    if do_prewhiten:
            image = prewhiten(image)
    #start_time = time.time()
    # resize and keep aspect ratio tf.resize_with_pad
#     with tf.device('/cpu:0'):
#         tfresized=tf.image.resize_image_with_pad(img, image_size, image_size)
    #with sess.as_default():
     #   image=tf.image.resize_image_with_pad(image, image_size, image_size).eval()
      #  
    image=resize_with_pad(image, image_size)
    #plt.figure()
    #plt.imshow(image.astype(np.uint8))
    #plt.show()
    #print('time elapsed[img_pro]:', time.time() - start_time, ' seconds')
    return image.reshape(1,image_size,image_size,3)

def load_model(model, input_map=None):
    # Check if the model is a model directory (containing a metagraph and a checkpoint file)
    #  or if it is a protobuf file with a frozen graph
    model_exp = os.path.expanduser(model)
    if (os.path.isfile(model_exp)):
        print('Model filename: %s' % model_exp)
        with gfile.FastGFile(model_exp,'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            
            tf.import_graph_def(graph_def, input_map=input_map, name='')
    else:
        print('Model directory: %s' % model_exp)
        meta_file, ckpt_file = get_model_filenames(model_exp)
        
        print('Metagraph file: %s' % meta_file)
        print('Checkpoint file: %s' % ckpt_file)
        saver = tf.train.import_meta_graph(os.path.join(model_exp, meta_file), input_map=input_map)
        saver.restore(tf.get_default_session(), os.path.join(model_exp, ckpt_file))

def get_model_filenames(model_dir):
    files = os.listdir(model_dir)
    meta_files = [s for s in files if s.endswith('.meta')]
    if len(meta_files)==0:
        raise ValueError('No meta file found in the model directory (%s)' % model_dir)
    elif len(meta_files)>1:
        raise ValueError('There should not be more than one meta file in the model directory (%s)' % model_dir)
    meta_file = meta_files[0]
    ckpt = tf.train.get_checkpoint_state(model_dir)
    if ckpt and ckpt.model_checkpoint_path:
        ckpt_file = os.path.basename(ckpt.model_checkpoint_path)
        return meta_file, ckpt_file

    meta_files = [s for s in files if '.ckpt' in s]
    max_step = -1
    for f in files:
        step_str = re.match(r'(^model-[\w\- ]+.ckpt-(\d+))', f)
        if step_str is not None and len(step_str.groups())>=2:
            step = int(step_str.groups()[1])
            if step > max_step:
                max_step = step
                ckpt_file = step_str.groups()[0]
    return meta_file, ckpt_file
    
def read_img(image_file):
    img = cv2.imread(image_file)
    b,g,r = cv2.split(img)           # get b, g, r
    img = cv2.merge([r,g,b]) # switch it to r, g, b
    return img

def get_embeddings(img):
    #main logic start
    # read image
    result=detector.detect_faces(img)
    for item in result:
        #print (item)
        face=extract_face(img, np.maximum(0,item['box']))# get rid of negative coordingnates, probably due to the padding of mtcnn
        with sess.as_default():
            #start_time = time.time()
            fine_face = prep_img(face, image_size) 
            #print('time elapsed:', time.time() - start_time, ' seconds')       
            feed_dict = { images_placeholder:fine_face, phase_train_placeholder:False }
            start_time = time.time()
            eb = sess.run(embeddings, feed_dict=feed_dict)[0]
            #print('time elapsed[graph_emb]:', time.time() - start_time, ' seconds')
            item['embedding']= eb
            item['face_img'] = face
            # item['img_file'] = image_file
    #print('time elapsed:', time.time() - start_time, ' seconds')
    return result


#instanciating MTCNN
def init():
    global images_placeholder, embeddings, phase_train_placeholder, detector
    detector = MTCNN()
    # create a tensorflow session which persists throughout the entire process.
    model = "20180408-102900"
    with sess.as_default():
        # Get input and output tensors
        #images_placeholder = tf.image.resize_images(images_placeholder,(image_size,image_size))
        load_model(model)
        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]
@atexit.register
def goodbye():
    tf.session.close()
    print("closing session ... done")
