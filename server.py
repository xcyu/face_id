import flask
import numpy as np
# import tensorflow as tf
# from keras.models import load_model
# from keras.utils import multi_gpu_model
import os
import cv2
import redis
import fd
import pandas as pd
from sklearn.neighbors import RadiusNeighborsClassifier


HOST = '0.0.0.0'  # Use host 0.0.0.0 to allow access from all devices
PORT = 8080
GPU=os.getenv('GPU', False)
# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = RadiusNeighborsClassifier(radius=0.62,outlier_label=-1)# 0.63 starts to show miss labeling
features=[]
labels=[]
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

#persis db
r = redis.Redis(host='redis', port=6379, db=0)

def allowed_file(filename):
    return '.' in filename and  filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def init():
    print('initiate face detection module')
    fd.init()
    global features, labels
    print('load imagedb ...')
    #imagedb is a key value pair(pydict)
    # { imguri: [{bb, embedding, for fac one},{},...{}]
    # }
    #TODO: check image hash to avoid duplicate
    for key in r.scan_iter():
        for patch in r[key]:
            features.append(patch['embedding'])
            labels.append(patch['label'])
    print('build prediction model')
    model.fit(features,labels)
    print('Initialize Engine ... Done!')

# Cross origin support
def sendResponse(responseObj):
    response = flask.jsonify(responseObj)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', 'GET')
    response.headers.add('Access-Control-Allow-Headers', 'accept,content-type,Origin,X-Requested-With,Content-Type,access_token,Accept,Authorization,source')
    response.headers.add('Access-Control-Allow-Credentials', True)
    return response

# API for prediction
@app.route("/predict", methods=["POST"])
def predict():
    request = flask.request
    filename=request.data
    print (filename)
    if not allowed_file(filename):
        return 'illegal filename!'
    filename=os.path.join(UPLOAD_FOLDER,filename)
    img=fd.read_img(filename)
    imhash=cv2wimg_hash_pHash(img).tostring()
    rec=r.get(imhash)
    if not rec:
        rec=pd.DataFrame.from_dict(fd.get_embeddings(img))
        rec['label']=model.predict(rec['embedding'].to_list())
        r.put(imhash,rec.to_dict())
        # return redirect(url_for('uploaded_file',
                                    # filename=filename))
    return sendResponse(rec.to_dict())
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''
@app.route('/uploads/<filename>')

def uploaded_file(filename):
        return send_from_directory(UPLOAD_FOLDER,filename)
# if this is the main thread of execution first load the model and then start the server
if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
"please wait until server has fully started"))
    init()
    app.run(HOST, PORT, threaded=True)

